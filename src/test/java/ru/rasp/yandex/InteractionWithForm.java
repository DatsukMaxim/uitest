package ru.rasp.yandex;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class InteractionWithForm {

    private WebDriver driver;

    public InteractionWithForm(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "from")
    private WebElement from;
    @FindBy(id = "to")
    private WebElement to;
    @FindBy(id = "when")
    private WebElement when;
    @FindBy(xpath = "//span[@class='Button__title'][contains(.,'Найти')]")
    private WebElement searchButton;
    @FindBy(xpath = "//span[@class='RadioButton__buttonLable'][contains(.,'Автобус')]")
    private WebElement busButton;
    @FindBy(xpath = "/html/body/div[1]/div/main/div/div[1]/div[1]/div/div[2]/div[2]")
    public WebElement textError;
    @FindBy(className = "SearchSegment_isVisible")
    public List<WebElement> SearchSegment_isVisible;

    @Step("Ввод данных")
    public void fieldsFilling(String strFrom, String strTo, String strWhen){
        from.clear();
        from.sendKeys(strFrom);
        to.sendKeys(strTo);
        when.sendKeys(strWhen);
        searchButton.click();
    }

    @Step("Нажате кнопки 'Автобус'")
    public void busButtonClick() {
        busButton.click();
    }

    @Step("Поиск элементов")
    public void findElement(String name) {
        for (WebElement Element : driver.findElements(By.className(name)))
            assertNotNull(driver.findElement(By.className(name)));
    }

    public void waiting(String element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className(element)));
    }

    @Step("Подсчет элементов")
    public int countElements() {
        int count = 0;
        for (WebElement Element: SearchSegment_isVisible)
            count++;
        return count;
    }
}
