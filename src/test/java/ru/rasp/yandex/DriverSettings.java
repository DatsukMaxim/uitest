package ru.rasp.yandex;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URI;

public class DriverSettings {

    public static RemoteWebDriver initDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("firefox");
        capabilities.setVersion("67.0");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);

        RemoteWebDriver driver = new RemoteWebDriver(
                URI.create("http://0.0.0.0:4444/wd/hub").toURL(),
                capabilities
        );
        driver.manage().window().maximize();
        driver.navigate().to("https://rasp.yandex.ru");
        return driver;
    }
}
