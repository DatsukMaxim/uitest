package ru.rasp.yandex;

import org.junit.jupiter.api.Test;
import java.net.MalformedURLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class YandexTest extends DriverSettings {

    InteractionWithForm header;
    public YandexTest() throws MalformedURLException {
        header = new InteractionWithForm(initDriver());
    }

    @Test
    public void testCase1() {
        header.fieldsFilling("Кемерово", "Москва", "7 июля");
        header.waiting("SearchSegment_isVisible");
        header.findElement("TransportIcon_icon");
        header.findElement("SegmentTitle_title");
        header.findElement("SearchSegment_duration");
        header.countElements();
        assertEquals(14, header.countElements());
    }

    @Test
    public void testCase2() {
        header.busButtonClick();
        header.fieldsFilling("Кемерово проспект Ленина", "Кемерово Бакинский переулок", "Среда");
        header.waiting("ErrorPageSearchForm__titleContainer");
        assertTrue(header.textError.getText().equals("Пункт прибытия не найден. Проверьте правильность написания или выберите другой город."));
    }
}
